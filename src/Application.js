import React from "react";
import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import { all } from "redux-saga/effects";
import createSagaMiddleware from "redux-saga";

import applicationStateReducer from "./state/reducers";
import applicationSagas from "./state/sagas";
import Routes from "./views/pages/Routes";
import Header from "./views/components/Header";
import Footer from "./views/components/Footer";

import "./Application.scss";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  applicationStateReducer,
  applyMiddleware(sagaMiddleware)
);

function* applicationSaga() {
  yield all([...applicationSagas]);
}

sagaMiddleware.run(applicationSaga);

export default function Application() {
  return (
    <Provider store={store}>
      <Header />
      <Routes />
      <Footer />
    </Provider>
  );
}
