import React from "react";
import PropTypes from "prop-types";
import { Link } from "@reach/router";

import NoCoverImage from "../../assets/images/no-cover-available.png";

import "./SearchResultListItem.scss";

SearchResultListItem.propTypes = {
  data: PropTypes.object.isRequired
};

export default function SearchResultListItem({ data }) {
  const { id, name, image } = data;

  return (
    <li className="rtl-search-results-list-item" key={id}>
      <Link className="rtl-search-results-list-item-link" to={`/show/${id}`}>
        <img src={image ? image["medium"] : NoCoverImage} alt={name} />
        <div className="rtl-search-results-list-item-name">{name}</div>
      </Link>
    </li>
  );
}
