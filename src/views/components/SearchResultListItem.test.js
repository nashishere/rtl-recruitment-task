import React from "react";
import { shallow } from "enzyme";

import SearchResultListItem from "./SearchResultListItem";

const defaultProps = {
  data: {
    id: 1234,
    name: "Powerpuff Girls",
    image: null
  }
};

describe("SearchResultListItem component", () => {
  it("renders without crashing", () => {
    shallow(<SearchResultListItem {...defaultProps} />);
  });

  it("renders correctly with default params", () => {
    const wrapper = shallow(<SearchResultListItem {...defaultProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
