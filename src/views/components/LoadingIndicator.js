import React from "react";
import { FaCogs } from "react-icons/fa";

import "./LoadingIndicator.scss";

export default function LoadingIndicator() {
  return (
    <div className="rtl-loading-indicator">
      <FaCogs />
      <span>Loading...</span>
    </div>
  );
}
