import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";

import "./SearchInput.scss";

SearchInput.propTypes = {
  initialValue: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
};

export default function SearchInput({ initialValue = "", onSubmit }) {
  const inputRef = useRef();
  const [value, setValue] = useState(initialValue);

  // Give the focus to the search field on mount for better user experience
  useEffect(() => {
    inputRef.current.focus();
  }, []);

  // Change the value of input with initialValue if initialValue has been changed
  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  function handleValueChange(event) {
    event.preventDefault();
    setValue(event.currentTarget.value);
  }

  function handleOnSubmit(event) {
    event.preventDefault();
    onSubmit(value.trim());
  }

  return (
    <form className="rtl-search-input" onSubmit={handleOnSubmit}>
      <input
        className="rtl-search-input-field"
        ref={inputRef}
        type="search"
        placeholder="Type here..."
        value={value}
        onChange={handleValueChange}
      />

      <input
        className="rtl-search-input-button"
        type="submit"
        defaultValue="Search"
      />
    </form>
  );
}
