import React from "react";
import { shallow } from "enzyme";

import LoadingIndicator from "./LoadingIndicator";

describe("LoadingIndicator component", () => {
  it("renders without crashing", () => {
    shallow(<LoadingIndicator />);
  });

  it("renders correctly with default params", () => {
    const wrapper = shallow(<LoadingIndicator />);
    expect(wrapper).toMatchSnapshot();
  });
});
