import React from "react";
import { Link } from "@reach/router";

import LogoImage from "../../assets/images/logo.png";

import "./Header.scss";

export default function Header(props) {
  return (
    <div className="rtl-header">
      <Link className="rtl-header-logo" to="/">
        <img src={LogoImage} alt="RTL" />
      </Link>
    </div>
  );
}
