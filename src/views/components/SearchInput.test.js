import React from "react";
import { shallow } from "enzyme";

import SearchInput from "./SearchInput";

const defaultProps = {
  onSubmit: () => {},
  initialValue: ""
};

describe("SearchInput component", () => {
  it("renders without crashing", () => {
    shallow(<SearchInput {...defaultProps} />);
  });

  it("renders correctly with default params", () => {
    const wrapper = shallow(<SearchInput {...defaultProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
