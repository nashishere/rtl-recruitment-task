import React from "react";
import PropTypes from "prop-types";

import SearchResultListItem from "./SearchResultListItem";

import "./SearchResultList.scss";

SearchResultList.propTypes = {
  items: PropTypes.array.isRequired
};

export default function SearchResultList({ items }) {
  return (
    <div className="rtl-search-results-list">
      <ul className="rtl-search-results-list-list">
        {items &&
          items.map(data => <SearchResultListItem key={data.id} data={data} />)}
      </ul>
    </div>
  );
}
