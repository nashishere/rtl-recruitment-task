import React from "react";
import { shallow } from "enzyme";

import SearchResultList from "./SearchResultList";

const defaultProps = {
  items: [
    {
      id: 1234,
      name: "Powerpuff Girls",
      image: null
    }
  ]
};

describe("SearchResultList component", () => {
  it("renders without crashing", () => {
    shallow(<SearchResultList {...defaultProps} />);
  });

  it("renders correctly with default params", () => {
    const wrapper = shallow(<SearchResultList {...defaultProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
