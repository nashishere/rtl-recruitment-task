import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import { useDocumentTitle } from "../../../utils/hooks";
import { PAGE_TITLES } from "../../../utils/constants";
import { applicationActionCreators } from "../../../state/actions";
import LoadingIndicator from "../../components/LoadingIndicator";
import NoCoverImage from "../../../assets/images/no-cover-available.png";

import "./DetailsPage.scss";

DetailsPage.propTypes = {
  showId: PropTypes.string
};

export default function DetailsPage({ showId, navigate }) {
  const dispatch = useDispatch();
  const selectedShow = useSelector(state => state.selectedShow);
  const isSelectedShowLoading = useSelector(
    state => state.isSelectedShowLoading
  );
  const selectedShowError = useSelector(state => state.selectedShowError);

  // This is a custom effect for changing document title
  useDocumentTitle(PAGE_TITLES.DETAILS(selectedShow && selectedShow.name));

  // Getting new show data if the showId prop has been changed
  useEffect(() => {
    const id = parseInt(showId, 10);
    dispatch(applicationActionCreators.setSelectedShowAction(id));
  }, [dispatch, showId]);

  function handleOnEpisodeClick(event) {
    event.preventDefault();

    const episodeId = event.currentTarget.dataset.episodeId;

    if (episodeId) {
      navigate(`/show/${showId}/episode/${episodeId}`);
    }
  }

  return (
    <div className="rtl-details-page">
      {isSelectedShowLoading && <LoadingIndicator />}
      {selectedShowError && (
        <div className="error">{selectedShowError.message}</div>
      )}

      {selectedShow && (
        <>
          <div className="rtl-details-page-details">
            <img
              src={
                selectedShow.image ? selectedShow.image["medium"] : NoCoverImage
              }
              alt={selectedShow.name}
            />
            <div className="rtl-details-page-details-info">
              <div className="rtl-details-page-details-info-header">
                <h3>{selectedShow.name}</h3>
                <h6>{selectedShow.type}</h6>
              </div>
              <div dangerouslySetInnerHTML={{ __html: selectedShow.summary }} />
            </div>
          </div>

          {selectedShow["_embedded"] && selectedShow["_embedded"]["episodes"] && (
            <div className="rtl-details-page-episodes">
              <h3>Episodes</h3>
              <table className="u-full-width">
                <thead>
                  <tr>
                    <th width="50">#</th>
                    <th>Episode Name</th>
                  </tr>
                </thead>
                <tbody>
                  {selectedShow["_embedded"]["episodes"].map(episode => (
                    <tr
                      className="rtl-details-page-episodes-item"
                      key={episode.id}
                      data-episode-id={episode.id}
                      onClick={handleOnEpisodeClick}
                    >
                      <td>{`#${episode.season}.${episode.number}`}</td>
                      <td>{episode.name}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </>
      )}
    </div>
  );
}
