import React from "react";
import { Router } from "@reach/router";

import MainPage from "./main/MainPage";
import DetailsPage from "./details/DetailsPage";
import EpisodeDetailPage from "./episodeDetail/EpisodeDetailPage";

import "./Routes.scss";

export default function Routes() {
  return (
    <Router className="rtl-routes">
      <MainPage path="/" />
      <MainPage path="/search/:searchQuery" />
      <DetailsPage path="show/:showId" />
      <EpisodeDetailPage path="show/:showId/episode/:episodeId" />
    </Router>
  );
}
