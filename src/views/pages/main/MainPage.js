import React, { useEffect, useMemo } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "@reach/router";

import { applicationActionCreators } from "../../../state/actions";
import { useDocumentTitle } from "../../../utils/hooks";
import { PAGE_TITLES, PRE_PICKED_SHOW_NAMES } from "../../../utils/constants";
import SearchInput from "../../components/SearchInput";
import SearchResultList from "../../components/SearchResultList";
import LoadingIndicator from "../../components/LoadingIndicator";

import "./MainPage.scss";

MainPage.propTypes = {
  searchQuery: PropTypes.string
};

export default function MainPage({ searchQuery = "", navigate }) {
  // This is a custom effect for changing document title
  useDocumentTitle(PAGE_TITLES.MAIN(searchQuery));

  const dispatch = useDispatch();
  const foundShows = useSelector(state => state.foundShows);
  const isSearchShowLoading = useSelector(state => state.isSearchShowLoading);
  const searchShowError = useSelector(state => state.searchShowError);

  // Creating and memoizing the found show list elements
  const foundShowsList = useMemo(() => {
    return <SearchResultList items={foundShows} />;
  }, [foundShows]);

  // Dispatch a new search action if searchQuery has been changed
  useEffect(() => {
    dispatch(applicationActionCreators.searchShowAction(searchQuery));
  }, [dispatch, searchQuery]);

  function handleSearchValueChange(value) {
    if (value && value !== "") {
      navigate(`/search/${encodeURI(value)}`);
    }
  }

  return (
    <div className="rtl-main-page">
      <h3>Search a show</h3>
      <SearchInput
        onSubmit={handleSearchValueChange}
        initialValue={searchQuery}
      />

      <h6>or pick from the list</h6>
      <ul className="rtl-main-page-link-list">
        {PRE_PICKED_SHOW_NAMES.map(name => (
          <li key={name}>
            <Link to={`/search/${encodeURI(name)}`}>{name}</Link>
          </li>
        ))}
      </ul>

      {isSearchShowLoading && <LoadingIndicator />}
      {searchShowError && (
        <div className="error">{searchShowError.message}</div>
      )}
      {foundShowsList}
    </div>
  );
}
