import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import { useDocumentTitle } from "../../../utils/hooks";
import { PAGE_TITLES } from "../../../utils/constants";
import { applicationActionCreators } from "../../../state/actions";
import LoadingIndicator from "../../components/LoadingIndicator";
import NoCoverImage from "../../../assets/images/no-cover-available.png";

import "./EpisodeDetailPage.scss";

EpisodeDetailPage.propTypes = {
  showId: PropTypes.string,
  episodeId: PropTypes.string
};

export default function EpisodeDetailPage({ showId, episodeId, navigate }) {
  const dispatch = useDispatch();
  const selectedShow = useSelector(state => state.selectedShow);
  const isSelectedShowLoading = useSelector(
    state => state.isSelectedShowLoading
  );
  const selectedShowError = useSelector(state => state.selectedShowError);

  let selectedEpisode;
  if (
    selectedShow &&
    selectedShow["_embedded"] &&
    selectedShow["_embedded"]["episodes"]
  ) {
    selectedEpisode = selectedShow["_embedded"]["episodes"].find(
      episode => episode.id === parseInt(episodeId, 10)
    );
  }

  // This is a custom effect for changing document title
  useDocumentTitle(PAGE_TITLES.EPISODE_DETAIL());

  // Getting new show data if the showId prop has been changed
  useEffect(() => {
    const id = parseInt(showId, 10);
    dispatch(applicationActionCreators.setSelectedShowAction(id));
  }, [dispatch, showId]);

  function handleBackToListClick(event) {
    event.preventDefault();
    navigate(`/show/${showId}`);
  }

  return (
    <div className="rtl-episode-detail-page">
      {isSelectedShowLoading && <LoadingIndicator />}
      {selectedShowError && (
        <div className="error">{selectedShowError.message}</div>
      )}

      {selectedEpisode && (
        <div className="rtl-episode-detail-page-details">
          <img
            src={
              selectedEpisode.image
                ? selectedEpisode.image["medium"]
                : NoCoverImage
            }
            alt={selectedEpisode.name}
          />
          <div className="rtl-episode-detail-page-info">
            <div className="rtl-episode-detail-page-info-header">
              <h3>{selectedEpisode.name}</h3>
              <h6>{`${selectedShow.name} - Season: ${selectedEpisode.season} Episode: ${selectedEpisode.number}`}</h6>
            </div>
            <div
              dangerouslySetInnerHTML={{
                __html: selectedEpisode.summary || "No information"
              }}
            />
            <button onClick={handleBackToListClick}>Back to list</button>
          </div>
        </div>
      )}
    </div>
  );
}
