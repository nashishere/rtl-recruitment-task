export const APPLICATION_ACTION_TYPES = {
  SEARCH_SHOW: "SEARCH_SHOW",
  SEARCH_SHOW_FULFILLED: "SEARCH_SHOW_FULFILLED",
  SEARCH_SHOW_FAILED: "SEARCH_SHOW_FAILED",

  SET_SELECTED_SHOW: "SET_SELECTED_SHOw",
  SET_SELECTED_SHOW_FULFILLED: "SET_SELECTED_SHOw_FULFILLED",
  SET_SELECTED_SHOW_FAILED: "SET_SELECTED_SHOw_FAILED"
};

export const PAGE_TITLES = {
  MAIN: searchQuery =>
    searchQuery ? `RTL - Search for "${searchQuery}"` : "RTL",
  DETAILS: name => (name ? `RTL - ${name}` : "RTL"),
  EPISODE_DETAIL: (name, episodeName) =>
    name && episodeName ? `RTL - ${name} - ${episodeName}` : "RTL"
};

export const PRE_PICKED_SHOW_NAMES = [
  "Powerpuff Girls",
  "Totally Spies!",
  "Kim Possible"
];
