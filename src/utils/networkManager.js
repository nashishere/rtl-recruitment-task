import axios from "axios";

const instance = axios.create({
  baseURL: `https://api.tvmaze.com/`,
  maxRedirects: 0
});

export const networkManager = {
  searchShowByName: query => {
    return instance.get("search/shows", {
      params: {
        q: query
      },
      transformResponse: data => {
        // Sanitizing the data a little bit here
        const serializedData = JSON.parse(data);
        const convertedData = serializedData.map(i => i.show);

        return convertedData;
      }
    });
  },

  getShowById: id => {
    return instance.get(`/shows/${id}?embed=episodes`);
  }
};
