import applicationReducer, { applicationInitialState } from "./reducers";
import { applicationActionCreators } from "./actions";

describe("Reducers", () => {
  const error = new Error("An error occured!");
  const query = "Powerpuff Girls";
  const id = 12345;
  const response = [];

  it("should return the initial state", () => {
    expect(applicationReducer(undefined, {})).toEqual(applicationInitialState);
  });

  it("should handle APPLICATION_ACTION_TYPES.SEARCH_SHOW action", () => {
    expect(
      applicationReducer(
        undefined,
        applicationActionCreators.searchShowAction(query)
      )
    ).toEqual({
      ...applicationInitialState,
      isSearchShowLoading: true,
      searchShowError: null,
      foundShows: []
    });
  });

  it("should handle APPLICATION_ACTION_TYPES.SEARCH_SHOW_FULFILLED action", () => {
    expect(
      applicationReducer(
        undefined,
        applicationActionCreators.searchShowFulfilledAction(response)
      )
    ).toEqual({
      ...applicationInitialState,
      isSearchShowLoading: false,
      searchShowError: null,
      foundShows: response
    });
  });

  it("should handle APPLICATION_ACTION_TYPES.SEARCH_SHOW_FAILED action", () => {
    expect(
      applicationReducer(
        undefined,
        applicationActionCreators.searchShowFailedAction(error)
      )
    ).toEqual({
      ...applicationInitialState,
      isSearchShowLoading: false,
      searchShowError: error,
      foundShows: []
    });
  });

  it("should handle APPLICATION_ACTION_TYPES.SET_SELECTED_SHOw action", () => {
    expect(
      applicationReducer(
        undefined,
        applicationActionCreators.setSelectedShowAction(id)
      )
    ).toEqual({
      ...applicationInitialState,
      isSelectedShowLoading: true,
      selectedShowError: null
    });
  });

  it("should handle APPLICATION_ACTION_TYPES.SET_SELECTED_SHOw_FULFILLED action", () => {
    expect(
      applicationReducer(
        undefined,
        applicationActionCreators.setSelectedShowFulfilledAction(response)
      )
    ).toEqual({
      ...applicationInitialState,
      isSelectedShowLoading: false,
      selectedShowError: null,
      selectedShow: response
    });
  });

  it("should handle APPLICATION_ACTION_TYPES.SET_SELECTED_SHOw_FAILED action", () => {
    expect(
      applicationReducer(
        undefined,
        applicationActionCreators.setSelectedShowFailedAction(error)
      )
    ).toEqual({
      ...applicationInitialState,
      isSelectedShowLoading: false,
      selectedShowError: error,
      selectedShow: null
    });
  });
});
