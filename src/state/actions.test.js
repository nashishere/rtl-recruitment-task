import { applicationActionCreators } from "./actions";
import { APPLICATION_ACTION_TYPES } from "../utils/constants";

describe("Actions", () => {
  const error = new Error("An error occured!");
  const query = "Powerpuff Girls";
  const id = 12345;
  const response = [];

  it("should create an action to search show with the stated payload", () => {
    expect(applicationActionCreators.searchShowAction(query)).toEqual({
      type: APPLICATION_ACTION_TYPES.SEARCH_SHOW,
      payload: {
        query
      }
    });
  });

  it("should create an action to search show is fulfilled with the stated payload", () => {
    expect(
      applicationActionCreators.searchShowFulfilledAction(response)
    ).toEqual({
      type: APPLICATION_ACTION_TYPES.SEARCH_SHOW_FULFILLED,
      payload: {
        response
      }
    });
  });

  it("should create an action to search show is failed with the stated payload", () => {
    expect(applicationActionCreators.searchShowFailedAction(error)).toEqual({
      type: APPLICATION_ACTION_TYPES.SEARCH_SHOW_FAILED,
      payload: {
        error
      }
    });
  });

  it("should create an action to set selected show with the stated payload", () => {
    expect(applicationActionCreators.setSelectedShowAction(id)).toEqual({
      type: APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW,
      payload: {
        id
      }
    });
  });

  it("should create an action to set selected show is fulfilled with the stated payload", () => {
    expect(
      applicationActionCreators.setSelectedShowFulfilledAction(response)
    ).toEqual({
      type: APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW_FULFILLED,
      payload: {
        response
      }
    });
  });

  it("should create an action to set selected show is failed with the stated payload", () => {
    expect(
      applicationActionCreators.setSelectedShowFailedAction(error)
    ).toEqual({
      type: APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW_FAILED,
      payload: {
        error
      }
    });
  });
});
