import { APPLICATION_ACTION_TYPES } from "../utils/constants";

export const applicationInitialState = {
  foundShows: [],
  isSearchShowLoading: false,
  searchShowError: null,

  selectedShow: null,
  isSelectedShowLoading: false,
  selectedShowError: null
};

export default function(state = applicationInitialState, action) {
  let newState;

  switch (action.type) {
    case APPLICATION_ACTION_TYPES.SEARCH_SHOW: {
      newState = {
        ...state,
        isSearchShowLoading: true,
        searchShowError: null,
        foundShows: []
      };

      break;
    }

    case APPLICATION_ACTION_TYPES.SEARCH_SHOW_FULFILLED: {
      newState = {
        ...state,
        isSearchShowLoading: false,
        searchShowError: null,
        foundShows: action.payload.response
      };

      break;
    }

    case APPLICATION_ACTION_TYPES.SEARCH_SHOW_FAILED: {
      newState = {
        ...state,
        isSearchShowLoading: false,
        searchShowError: action.payload.error,
        foundShows: []
      };

      break;
    }

    case APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW: {
      newState = {
        ...state,
        isSelectedShowLoading: true,
        selectedShowError: null
      };

      break;
    }

    case APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW_FULFILLED: {
      newState = {
        ...state,
        isSelectedShowLoading: false,
        selectedShowError: null,
        selectedShow: action.payload.response
      };

      break;
    }

    case APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW_FAILED: {
      newState = {
        ...state,
        isSelectedShowLoading: false,
        selectedShowError: action.payload.error,
        selectedShow: null
      };

      break;
    }

    default: {
      newState = state;

      break;
    }
  }

  return newState;
}
