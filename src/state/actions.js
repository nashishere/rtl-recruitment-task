import { APPLICATION_ACTION_TYPES } from "../utils/constants";

const searchShowAction = query => {
  return {
    type: APPLICATION_ACTION_TYPES.SEARCH_SHOW,
    payload: {
      query
    }
  };
};

const searchShowFulfilledAction = response => {
  return {
    type: APPLICATION_ACTION_TYPES.SEARCH_SHOW_FULFILLED,
    payload: {
      response
    }
  };
};

const searchShowFailedAction = error => {
  return {
    type: APPLICATION_ACTION_TYPES.SEARCH_SHOW_FAILED,
    payload: {
      error
    }
  };
};

const setSelectedShowAction = id => {
  return {
    type: APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW,
    payload: {
      id
    }
  };
};

const setSelectedShowFulfilledAction = response => {
  return {
    type: APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW_FULFILLED,
    payload: {
      response
    }
  };
};

const setSelectedShowFailedAction = error => {
  return {
    type: APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW_FAILED,
    payload: {
      error
    }
  };
};

export const applicationActionCreators = {
  searchShowAction,
  searchShowFulfilledAction,
  searchShowFailedAction,
  setSelectedShowAction,
  setSelectedShowFulfilledAction,
  setSelectedShowFailedAction
};
