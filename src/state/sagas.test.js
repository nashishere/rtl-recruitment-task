import { call, put, select } from "redux-saga/effects";

import { searchShowSaga, setSelectedShowSaga } from "./sagas";
import { networkManager } from "../utils/networkManager";
import { applicationActionCreators } from "./actions";

describe("Sagas", () => {
  const query = "query";
  const id = 123;
  const emptyQuery = "";
  const response = [{}, {}, {}];
  const emptyResponse = [];
  const error = new Error();

  it("search shows successfully with query", async () => {
    const mockAction = {
      payload: {
        query: query
      }
    };

    const mockResponse = {
      data: response
    };

    const generator = searchShowSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(networkManager.searchShowByName, query));

    result = generator.next(mockResponse);
    expect(result.value).toEqual(
      put(applicationActionCreators.searchShowFulfilledAction(response))
    );
  });

  it("search shows successfully with empty query", async () => {
    const mockAction = {
      payload: {
        query: emptyQuery
      }
    };

    const generator = searchShowSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(
      put(applicationActionCreators.searchShowFulfilledAction(emptyResponse))
    );
  });

  it("fail searching shows", async () => {
    const mockAction = {
      payload: {
        query: query
      }
    };

    const generator = searchShowSaga(mockAction);
    let result;

    result = generator.next(mockAction);
    expect(result.value).toEqual(call(networkManager.searchShowByName, query));

    result = generator.throw(error);
    expect(result.value).toEqual(
      put(applicationActionCreators.searchShowFailedAction(error))
    );
  });

  it("search set active show successfully with query", async () => {
    const mockAction = {
      payload: {
        id: id
      }
    };

    const mockResponse = {
      data: response
    };

    const generator = setSelectedShowSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(select());

    result = generator.next(mockAction);
    expect(result.value).toEqual(call(networkManager.getShowById, id));

    result = generator.next(mockResponse);
    expect(result.value).toEqual(
      put(applicationActionCreators.setSelectedShowFulfilledAction(response))
    );
  });

  it("fail setting active show", async () => {
    const mockAction = {
      payload: {
        id: id
      }
    };

    const generator = setSelectedShowSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(select());

    result = generator.throw(error);
    expect(result.value).toEqual(
      put(applicationActionCreators.setSelectedShowFailedAction(error))
    );
  });
});
