import { takeEvery, call, put, select } from "redux-saga/effects";

import { applicationActionCreators } from "./actions";
import { APPLICATION_ACTION_TYPES } from "../utils/constants";
import { networkManager } from "../utils/networkManager";

export function* searchShowSaga({ payload: { query } }) {
  try {
    if (query === "") {
      yield put(applicationActionCreators.searchShowFulfilledAction([]));
    } else {
      const response = yield call(networkManager.searchShowByName, query);
      yield put(
        applicationActionCreators.searchShowFulfilledAction(response.data)
      );
    }
  } catch (error) {
    yield put(applicationActionCreators.searchShowFailedAction(error));
  }
}

export function* setSelectedShowSaga({ payload: { id } }) {
  try {
    const state = yield select();
    const selectedShow = state.selectedShow;

    if (selectedShow && selectedShow.id === id) {
      yield put(
        applicationActionCreators.setSelectedShowFulfilledAction(selectedShow)
      );
    } else {
      const response = yield call(networkManager.getShowById, id);
      yield put(
        applicationActionCreators.setSelectedShowFulfilledAction(response.data)
      );
    }
  } catch (error) {
    yield put(applicationActionCreators.setSelectedShowFailedAction(error));
  }
}

export function* watchSearchShowAction() {
  yield takeEvery(APPLICATION_ACTION_TYPES.SEARCH_SHOW, searchShowSaga);
}

export function* watchSetSelectedShowAction() {
  yield takeEvery(
    APPLICATION_ACTION_TYPES.SET_SELECTED_SHOW,
    setSelectedShowSaga
  );
}

const applicationSagas = [
  watchSearchShowAction(),
  watchSetSelectedShowAction()
];

export default applicationSagas;
